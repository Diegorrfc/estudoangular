import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';

import { NavbarComponent } from './navbar/navbar.component';
import { TaskComponent } from './Tasks/tasks.component';
import { LearningBindingComponent } from './learning-bindings/learning-bindings.component';
import { TaskDetailComponent } from './Tasks/task-detail/task-detail.component';
import { TaskService } from './Tasks/shared/task.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryTaskDataService } from './Tasks/in-memory-task-data.service';
import { LearnObservables } from './learning-observables.service';




@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    TaskComponent,
    LearningBindingComponent,
    TaskDetailComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    InMemoryWebApiModule.forRoot(InMemoryTaskDataService),

  ],
  providers: [TaskService, LearnObservables],
  bootstrap: [AppComponent]
})
export class AppModule { }
