import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TaskComponent } from './Tasks/tasks.component';
import { TaskDetailComponent } from './Tasks/task-detail/task-detail.component';
import { DashboardComponent } from './dashboard/dashboard.component';


const ROUTE = RouterModule.forRoot(
  [
    { path: 'dashboard', component: DashboardComponent },
    { path: 'task', component: TaskComponent },
    { path: 'task/:id', component: TaskDetailComponent },
    { path: '', redirectTo: 'task', pathMatch: 'full' }
  ]
);


@NgModule({
  imports: [ROUTE],
  exports: [RouterModule]

})

export class AppRoutingModule {

}
