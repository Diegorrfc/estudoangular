import { Component } from '@angular/core';

@Component({
    selector: 'app-learning-binding',
    templateUrl: './learning-binding.component.html'
})

export class LearningBindingComponent {

    public mouseClickCount: number;
    public mouseOverCount: number;
    public userName: string;
    public userEmail: string;

    /**
     *
     */
    constructor() {
        this.mouseClickCount = 0;
        this.mouseOverCount = 0;
        this.userName = '';
        this.userEmail = '';

    }

    public onClick() {
        console.log('Evento onclick');
        this.mouseClickCount++;

    }
    public onMouseOver() {
        console.log('Mouse Over');
        this.mouseOverCount++;
    }
    public keyDown(event: KeyboardEvent) {
        console.log(event);
        console.log('keyDown');
    }
    public keyUp() {
        console.log('keyUp');
    }

}
