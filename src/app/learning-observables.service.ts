
import { Injectable } from '@angular/core';
import { TaskService } from './Tasks/shared/task.service';
import { HttpClient } from '@angular/common/http';


import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';





@Injectable()
export class LearnObservables {

    /**
     *
     */
    constructor(private taskService: TaskService, private Http: HttpClient) {

    }


    learning() {
        this.Http.get('api/tasks').pipe(
            catchError(error => {
                console.log('salvando');
                return Observable.throw('detes');
            }))
            .subscribe(
                (sucess) => { console.log('suces', sucess); },
                (error) => { console.log('error', error); },
                () => { console.log('finalizado'); }
            );
    }
}
