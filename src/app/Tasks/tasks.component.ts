import { Component, OnInit } from '@angular/core';
import { Task } from './shared/task.model';
import { TaskService } from './shared/task.service';



@Component({
    selector: 'app-tasks',
    templateUrl: 'tasks-component.html',
    providers: [{ provide: TaskService, useClass: TaskService }]

})
export class TaskComponent implements OnInit {
    public tasks: Array<Task>;
    public taskSele;
    public t;

    /**
     *
     */
    constructor(private taskService: TaskService) {


    }
    ngOnInit(): void {
        this.t = this.taskService.getTasks()
            .then((x) => { console.log(x); console.log('sucess'); this.tasks = x; })
            .catch(() => console.log('chorou'));
    }
    selectedItem(item: Task) {
        this.taskSele = item;
    }

}
