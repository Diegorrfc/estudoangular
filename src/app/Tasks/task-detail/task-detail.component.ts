import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { switchMap, catchError } from 'rxjs/operators';
import { Location } from '@angular/common';

import { Task } from '../shared/task.model';
import { TaskService } from '../shared/task.service';


@Component({
    selector: 'app-task-detail',
    templateUrl: './task-detail.component.html'

})

export class TaskDetailComponent implements OnInit {
    public task: Task;
    /**
     *
     */
    constructor(private taskService: TaskService, private activateRoute: ActivatedRoute, private loc: Location) {
        // this.task = new Task();
        // this.task.id = '1';
        // this.task.title = 'title';

    }
    ngOnInit() {


        this.activateRoute.params.pipe(
            switchMap((parmas: Params) => this.taskService.getTask(+parmas.id)))
            .subscribe((task) => this.task = task);


        // t his.activateRoute.params.subscribe((param: Params) => {
        //   this.taskService.getTask(+param.id).then(task => this.task = task);
        // });
    }
    goBack() {
        this.loc.back();
    }

}
