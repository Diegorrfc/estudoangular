import { Injectable } from '@angular/core';
import { Task } from './task.model';
import { HttpClient } from '@angular/common/http';



const TASK: Array<Task> = [
    { id: 3, title: 'Fazer tarefa 1' },
    { id: 4, title: 'Fazer tarefa 2' },
    { id: 5, title: 'Fazer tarefa 3' },
    { id: 6, title: 'Fazer tarefa 4' },
    { id: 7, title: 'Fazer tarefa 5' },
    { id: 8, title: 'Fazer tarefa 6' },
    { id: 7, title: 'Fazer tarefa 7' }
];
@Injectable()

export class TaskService {
    /**
     *
     */
    constructor(private http: HttpClient) {
    }

    public getTasks(): Promise<Task[]> {

       // this.http.get('api/tasks');
        const promise = new Promise<Task[]>(
            (resolve, reject) => {
                if (TASK.length > 0) {
                    setTimeout(() => {
                        resolve(TASK);
                    }, 50);
                } else {
                    reject('Fazer tarefa 4');
                }
            });
        return promise;
    }
    public getDashTask(): Promise<Task[]> {
        return Promise.resolve(TASK.slice(0, 3));
    }
    public getTask(id: number): Promise<Task> {
        console.log(id);
        return this.getTasks().then((task: Task[]) => task.find(t => t.id === id));
    }

}
