import { Component, OnInit } from '@angular/core';

import { TaskService } from '../Tasks/shared/task.service';
import { Task } from '../Tasks/shared/task.model';
import { LearnObservables } from '../learning-observables.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html'
})

export class DashboardComponent implements OnInit {

    /**
     *
     */
    public tasks: Task[];
    constructor(private taskService: TaskService, private lear: LearnObservables) { }

    ngOnInit() {
        this.lear.learning();
        this.taskService.getDashTask().then((tasks) => this.tasks = tasks);
    }
}
